import java.io.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;  
import java.lang.Math;


public class test
{
public static void main(String[] args) {
    int num[] = {5, -3, 0, -18, 1, 2, 0};

    int temp = 0;
    boolean finished = false;

    do{
        finished = true; // This will stay true if nothing needs to be changed in your array.
        for (int i = 0 ; i < num.length - 1 ; i++){
            if (num[i] > num[i+1]){
                finished = false; // Can not go off the loop if it is not sorted yet.
                temp = num[i]; // Interchanging of array's indexes
                num[i] = num[i+1];
                num[i+1] = temp;
            }
        }
    } while(!finished);

    System.out.println(Arrays.toString(num));
}
}
