# MajorityConsensus

This application computes election results using the Majority Consensus method.
It takes a `.csv` file containing the sum of all the votes for each candidate as
parameter, such as:

```csv
Candidates,Reject,Poor,Acceptable,Good,VeryGood,Excellent
Y,20,10,10,20,10,30
Z,20,40,10,10,15,5
X,20,20,30,10,15,5
A,15,15,15,15,15,25
K,30,20,10,10,15,5
```

and return the results in a generated `.txt` file, such as:

```tsv
Candidates	Grades	0
Z	75	1
K	72	2
X	-5	3
Y	-20	4
A	-25	5
```

It can be run directly:

```
java MajorityConsensus.java example/MC.csv
```

or be compiled to bytecode and run:

```
javac MajorityConsensus.java
java MajorityConsensus example/MC.csv
```
